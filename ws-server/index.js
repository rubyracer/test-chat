const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);


io.on('connection', function(socket){
  socket.on('message', function(data){
    io.emit('chat-' + data.chat + '/message', data)
  });

  socket.on('member', function(data){
    io.emit('chat-' + data.chat + '/member', data)
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
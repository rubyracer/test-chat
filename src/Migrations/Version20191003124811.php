<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191003124811 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE base_chat_message (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', chat_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', author_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME DEFAULT NULL, INDEX IDX_49DCA3CE1A9A7125 (chat_id), INDEX IDX_49DCA3CEF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chat_member (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', chat_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME DEFAULT NULL, INDEX IDX_1738CD591A9A7125 (chat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chat (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE base_chat_message ADD CONSTRAINT FK_49DCA3CE1A9A7125 FOREIGN KEY (chat_id) REFERENCES chat (id)');
        $this->addSql('ALTER TABLE base_chat_message ADD CONSTRAINT FK_49DCA3CEF675F31B FOREIGN KEY (author_id) REFERENCES chat_member (id)');
        $this->addSql('ALTER TABLE chat_member ADD CONSTRAINT FK_1738CD591A9A7125 FOREIGN KEY (chat_id) REFERENCES chat (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE base_chat_message DROP FOREIGN KEY FK_49DCA3CEF675F31B');
        $this->addSql('ALTER TABLE base_chat_message DROP FOREIGN KEY FK_49DCA3CE1A9A7125');
        $this->addSql('ALTER TABLE chat_member DROP FOREIGN KEY FK_1738CD591A9A7125');
        $this->addSql('DROP TABLE base_chat_message');
        $this->addSql('DROP TABLE chat_member');
        $this->addSql('DROP TABLE chat');
    }
}

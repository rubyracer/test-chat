<?php

namespace App\DTO\Chat;

use App\Entity\User;

class ChatMemberDTO
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var User
     */
    public $user;

    public function __construct(string $id, User $user)
    {
        $this->id = $id;
        $this->user = $user;
    }
}
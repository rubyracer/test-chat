<?php

namespace App\DTO\Chat;

use Doctrine\Common\Collections\Collection;

class ChatDetailDTO
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var array
     */
    public $members;

    /**
     * @var array
     */
    public $messages;

    public function __construct(string $id, array $members, array $messages)
    {
        $this->id = $id;
        $this->members = $members;
        $this->messages = $messages;
    }
}
<?php

namespace App\DTO\Chat;

class ChatListDTO
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var bool
     */
    public $isMember;

    public function __construct(string $id, string $name, bool $isMember)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isMember = $isMember;
    }
}
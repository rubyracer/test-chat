<?php

namespace App\Repository;

use App\Entity\Chat;
use App\Entity\ChatMember;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ChatMember|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChatMember|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChatMember[]    findAll()
 * @method ChatMember[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChatMemberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChatMember::class);
    }

    public function findChatMember(Chat $chat, User $user): ?ChatMember
    {
        return $this
            ->createQueryBuilder('chatMember')
            ->select('chatMember')
            ->where('chatMember.chat = :chat')
            ->setParameter('chat', $chat)
            ->andWhere('chatMember.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function save(ChatMember $chatMember)
    {
        $this->_em->persist($chatMember);
        $this->_em->flush($chatMember);
    }
}

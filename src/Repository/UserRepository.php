<?php

namespace App\Repository;

use App\Entity\User;
use App\Factory\UserFactory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User|null findOneByVisitorId(string $visitorId)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    private $factory;

    public function __construct(ManagerRegistry $registry, UserFactory $factory)
    {
        parent::__construct($registry, User::class);

        $this->factory = $factory;
    }

    public function findOrCreateByVisitorId(string $visitorId): User
    {
        $user = $this->findOneByVisitorId($visitorId);
        if (!$user instanceof User) {
            $user = $this->factory->create($visitorId);

            $this->save($user);
        }

        return $user;
    }

    public function save(User $user)
    {
        $this->_em->persist($user);
        $this->_em->flush($user);
    }
}

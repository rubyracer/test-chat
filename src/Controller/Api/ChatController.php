<?php

namespace App\Controller\Api;

use App\Entity\Chat;
use App\Factory\DTO\ChatDetailDTOFactory;
use App\Factory\DTO\ChatListDTOFactory;
use App\Repository\ChatRepository;
use App\Service\ChatMemberService;
use App\Service\ChatMessageService;
use App\Service\ChatService;
use App\Storage\UserStorage;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\RouteResource("chats", pluralize=false)
 */
class ChatController extends AbstractFOSRestController
{
    /**
     * @var UserStorage
     */
    private $userStorage;

    /**
     * @var ChatRepository
     */
    private $chatRepository;

    /**
     * @var ChatService
     */
    private $chatService;

    public function __construct(
        UserStorage $userStorage,
        ChatRepository $chatRepository,
        ChatService $chatService
    ) {
        $this->userStorage = $userStorage;
        $this->chatRepository = $chatRepository;
        $this->chatService = $chatService;
    }

    /**
     * @Rest\Get(path="chats")
     *
     * @Rest\View()
     */
    public function cgetAction(ChatListDTOFactory $factory)
    {
        $user = $this->userStorage->getUser();

        $data = array_map(function ($chat) use ($factory, $user) {
            return $factory->create($chat, $user);
        }, $this->chatRepository->findAll());

        return [
            'data' => $data,
        ];
    }

    /**
     * @Rest\Get(path="chats/{chat}")
     *
     * @Rest\View()
     */
    public function getAction(Chat $chat, ChatDetailDTOFactory $factory)
    {
        return $factory->create($chat);
    }

    /**
     * @Rest\Post(path="chats")
     *
     * @Rest\View()
     */
    public function postAction(ChatListDTOFactory $factory)
    {
        return $factory->create(
            $this->chatService->create($this->userStorage->getUser()),
            $this->userStorage->getUser()
        );
    }

    /**
     * @Rest\Post(path="chats/{chat}/join")
     *
     * @Rest\View()
     */
    public function postJoinAction(Chat $chat, ChatMemberService $chatMemberService)
    {
        $chatMemberService->joinToChat(
            $chat,
            $this->userStorage->getUser()
        );
    }

    /**
     * @Rest\Post(path="chats/{chat}/message")
     *
     * @Rest\View()
     */
    public function postMessageAction(Request $request, Chat $chat, ChatMessageService $chatMessageService)
    {
        $chatMessageService->sendMessageToChat(
            $chat,
            $this->userStorage->getUser(),
            $request->request->get('message')
        );
    }
}
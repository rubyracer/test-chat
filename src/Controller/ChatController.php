<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ChatController extends AbstractController
{
    public function getChats()
    {
        return $this->render('pages/chats.html.twig');
    }

    public function getChat(string $chatId)
    {
        return $this->render('pages/chat.html.twig', [
            'chatId' => $chatId,
        ]);
    }
}
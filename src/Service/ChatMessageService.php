<?php

namespace App\Service;

use App\Entity\Chat;
use App\Entity\ChatMessage;
use App\Entity\User;
use App\Factory\ChatMessageFactory;
use App\Repository\ChatMemberRepository;
use App\Repository\ChatMessageRepository;

class ChatMessageService
{
    private $chatMemberRepository;
    private $chatMessageRepository;
    private $chatMemberService;
    private $chatMessageFactory;

    public function __construct(
        ChatMemberRepository $chatMemberRepository,
        ChatMessageRepository $chatMessageRepository,
        ChatMemberService $chatMemberService,
        ChatMessageFactory $chatMessageFactory
    )
    {
        $this->chatMemberRepository = $chatMemberRepository;
        $this->chatMessageRepository = $chatMessageRepository;
        $this->chatMemberService = $chatMemberService;
        $this->chatMessageFactory = $chatMessageFactory;
    }

    public function sendMessageToChat(Chat $chat, User $user, string $message): ChatMessage
    {
        if (null === $chatMember = $this->chatMemberRepository->findChatMember($chat, $user)) {
            $chatMember = $this->chatMemberService->create($chat, $user);
        }

        $message = $this->chatMessageFactory->create($chat, $chatMember, $message);

        $this->chatMessageRepository->save($message);

        return $message;
    }
}
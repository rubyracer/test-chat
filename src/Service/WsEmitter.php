<?php

namespace App\Service;

use App\Entity\Chat;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;
use JMS\Serializer\SerializerInterface;

class WsEmitter
{
    /**
     * @var Client
     */
    private $websocketClient;

    /**
     * @var string
     */
    private $websocketUrl;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(string $websocketUrl, SerializerInterface $serializer)
    {
        $this->websocketUrl = $websocketUrl;
        $this->serializer = $serializer;
    }

    public function emit(string $eventName, Chat $chat, $data)
    {
        $this->getClient()->emit(
            $eventName,
            $this->serializer->toArray($data)
        );
    }

    private function getClient(): Client
    {
        if (!$this->websocketClient) {
            $this->websocketClient = new Client(
                new Version2X($this->websocketUrl)
            );
            $this->websocketClient->initialize();
        }

        return $this->websocketClient;
    }
}
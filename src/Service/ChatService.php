<?php

namespace App\Service;

use App\Entity\Chat;
use App\Entity\User;
use App\Factory\ChatFactory;
use App\Repository\ChatRepository;

class ChatService
{
    /**
     * @var ChatRepository
     */
    private $chatRepository;

    /**
     * @var ChatFactory
     */
    private $chatFactory;

    /**
     * @var ChatMemberService
     */
    private $chatMemberService;

    public function __construct(
        ChatRepository $chatRepository,
        ChatFactory $chatFactory,
        ChatMemberService $chatMemberService
    ) {
        $this->chatRepository = $chatRepository;
        $this->chatFactory = $chatFactory;
        $this->chatMemberService = $chatMemberService;
    }

    public function create(User $user): Chat
    {
        $chat = $this->chatFactory->create();
        $this->chatRepository->save($chat);

        $this->chatMemberService->joinToChat($chat, $user);

        return $chat;
    }
}
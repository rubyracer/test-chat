<?php

namespace App\Service;

use App\Entity\Chat;
use App\Entity\ChatMember;
use App\Entity\User;
use App\Factory\ChatMemberFactory;
use App\Repository\ChatMemberRepository;

class ChatMemberService
{
    /**
     * @var ChatMemberRepository
     */
    private $chatMemberRepository;

    /**
     * @var ChatMemberFactory
     */
    private $chatMemberFactory;

    public function __construct(ChatMemberRepository $chatMemberRepository, ChatMemberFactory $chatMemberFactory)
    {
        $this->chatMemberRepository = $chatMemberRepository;
        $this->chatMemberFactory = $chatMemberFactory;
    }

    public function create(Chat $chat, User $user): ChatMember
    {
        $chatMember = $this->chatMemberFactory->create($chat, $user);
        $this->chatMemberRepository->save($chatMember);

        return $chatMember;
    }

    public function joinToChat(Chat $chat, User $user): Chat
    {
        $chat->addMember(
            $this->create($chat, $user)
        );

        return $chat;
    }
}
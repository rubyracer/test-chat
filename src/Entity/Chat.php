<?php

namespace App\Entity;

use App\Entity\Timestampable\Timestampable;
use App\Entity\Timestampable\TimestampableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChatRepository")
 */
class Chat implements TimestampableInterface
{
    use \App\Entity\Uuid;
    use Timestampable;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChatMember", mappedBy="chat", cascade={"remove", "persist"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $members;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChatMessage", mappedBy="chat", cascade={"remove", "persist"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $messages;

    public function __construct(string $name)
    {
        $this->id = Uuid::uuid4()->toString();

        $this->name = $name;

        $this->members = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMember(ChatMember $chatMember)
    {
        if (!$this->members->contains($chatMember)) {
            $this->members->add($chatMember);
        }

        return $this;
    }

    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function isUserChatMember(User $user): bool
    {
        foreach ($this->members as $member) {
            if ($member->getUser() === $user) {
                return true;
            }
        }

        return false;
    }
}

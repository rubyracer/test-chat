<?php

namespace App\Entity;

use App\Entity\Timestampable\Timestampable;
use App\Entity\Timestampable\TimestampableInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements TimestampableInterface
{
    use \App\Entity\Uuid;
    use Timestampable;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $visitorId;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    public function __construct(string $visitorId, string $name)
    {
        $this->id = Uuid::uuid4()->toString();

        $this->visitorId = $visitorId;
        $this->name = $name;
    }

    public function getVisitorId(): string
    {
        return $this->visitorId;
    }

    public function getName(): string
    {
        return $this->name;
    }
}

<?php

namespace App\Entity;

use App\Entity\Timestampable\Timestampable;
use App\Entity\Timestampable\TimestampableInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChatMessageRepository")
 */
class ChatMessage implements TimestampableInterface
{
    use \App\Entity\Uuid;
    use Timestampable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Chat", inversedBy="messages")
     */
    protected $chat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ChatMember")
     */
    protected $author;

    /**
     * @ORM\Column(type="text")
     */
    protected $content;

    public function __construct(Chat $chat, ChatMember $member, string $content)
    {
        $this->id = Uuid::uuid4()->toString();

        $this->chat = $chat;
        $this->author = $member;
        $this->content = $content;
    }

    public function getChat(): Chat
    {
        return $this->chat;
    }

    public function getAuthor(): ChatMember
    {
        return $this->author;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}

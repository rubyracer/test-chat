<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Uuid
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid", unique=true)
     */
    private $id;

    public function getId(): string
    {
        return $this->id;
    }
}
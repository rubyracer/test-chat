<?php

namespace App\Entity;

use App\Entity\Timestampable\Timestampable;
use App\Entity\Timestampable\TimestampableInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChatMemberRepository")
 */
class ChatMember implements TimestampableInterface
{
    use \App\Entity\Uuid;
    use Timestampable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Chat", inversedBy="members")
     */
    private $chat;

    public function __construct(Chat $chat, User $user)
    {
        $this->id = Uuid::uuid4()->toString();

        $this->chat = $chat;
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getChat(): Chat
    {
        return $this->chat;
    }
}

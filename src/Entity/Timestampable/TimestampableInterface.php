<?php

namespace App\Entity\Timestampable;

interface TimestampableInterface
{
    public function getCreatedAt(): ?\DateTime;
    public function setCreatedAt(\DateTime $createdAt = null);
}
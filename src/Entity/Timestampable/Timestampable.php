<?php

namespace App\Entity\Timestampable;

trait Timestampable
{
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
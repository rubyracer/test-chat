<?php

namespace App\Factory\DTO;

use App\DTO\Chat\ChatMemberDTO;
use App\Entity\ChatMember;

class ChatMemberDTOFactory
{
    public function create(ChatMember $chatMember): ChatMemberDTO
    {
        return new ChatMemberDTO($chatMember->getId(), $chatMember->getUser());
    }
}
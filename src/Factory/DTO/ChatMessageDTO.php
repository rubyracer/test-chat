<?php

namespace App\Factory\DTO;

use App\DTO\Chat\ChatMemberDTO;

class ChatMessageDTO
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var ChatMemberDTO
     */
    public $author;

    /**
     * @var string
     */
    public $content;

    public function __construct(string  $id, ChatMemberDTO $author, string $content)
    {
        $this->id = $id;
        $this->author = $author;
        $this->content = $content;
    }
}
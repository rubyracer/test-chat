<?php

namespace App\Factory\DTO;

use App\DTO\Chat\ChatDetailDTO;
use App\Entity\Chat;

class ChatDetailDTOFactory
{
    /**
     * @var ChatMemberDTOFactory
     */
    private $chatMemberDTOFactory;

    /**
     * @var ChatMessageDTOFactory
     */
    private $chatMessageDTOFactory;

    public function __construct(ChatMemberDTOFactory $chatMemberDTOFactory, ChatMessageDTOFactory $chatMessageDTOFactory)
    {
        $this->chatMemberDTOFactory = $chatMemberDTOFactory;
        $this->chatMessageDTOFactory = $chatMessageDTOFactory;
    }

    public function create(Chat $chat): ChatDetailDTO
    {
        $members = array_map(function ($member) {
            return $this->chatMemberDTOFactory->create($member);
        }, $chat->getMembers()->toArray());

        $messages = array_map(function ($message) {
            return $this->chatMessageDTOFactory->create($message);
        }, $chat->getMessages()->toArray());

        return new ChatDetailDTO(
            $chat->getId(),
            $members,
            $messages
        );
    }
}
<?php

namespace App\Factory\DTO;

use App\Entity\ChatMessage;

class ChatMessageDTOFactory
{
    /**
     * @var ChatMemberDTOFactory
     */
    private $chatMemberDTOFactory;

    public function __construct(ChatMemberDTOFactory $chatMemberDTOFactory)
    {
        $this->chatMemberDTOFactory = $chatMemberDTOFactory;
    }

    public function create(ChatMessage $chatMessage): ChatMessageDTO
    {
        return new ChatMessageDTO(
            $chatMessage->getId(),
            $this->chatMemberDTOFactory->create($chatMessage->getAuthor()),
            $chatMessage->getContent()
        );
    }
}
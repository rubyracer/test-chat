<?php

namespace App\Factory\DTO;

use App\DTO\Chat\ChatListDTO;
use App\Entity\Chat;
use App\Entity\User;

class ChatListDTOFactory
{
    public function create(Chat $chat, User $user): ChatListDTO
    {
        return new ChatListDTO(
            $chat->getId(),
            $chat->getName(),
            $chat->isUserChatMember($user)
        );
    }
}
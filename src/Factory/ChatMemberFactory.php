<?php

namespace App\Factory;

use App\Entity\Chat;
use App\Entity\ChatMember;
use App\Entity\User;

class ChatMemberFactory
{
    public function create(Chat $chat, User $user): ChatMember
    {
        return new ChatMember($chat, $user);
    }
}
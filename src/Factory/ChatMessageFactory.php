<?php

namespace App\Factory;

use App\Entity\Chat;
use App\Entity\ChatMember;
use App\Entity\ChatMessage;

class ChatMessageFactory
{
    public function create(Chat $chat, ChatMember $member, string $content): ChatMessage
    {
        return new ChatMessage($chat, $member, $content);
    }
}
<?php

namespace App\Factory;

use App\Entity\User;

class UserFactory
{
    public function create(string $visitorId): User
    {
        return new User(
            $visitorId,
            sprintf('User - %s', uniqid())
        );
    }
}
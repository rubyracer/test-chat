<?php

namespace App\Factory;

use App\Entity\Chat;

class ChatFactory
{
    public function create(): Chat
    {
        return new Chat(
            sprintf('Chat - %s', uniqid())
        );
    }
}
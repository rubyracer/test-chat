<?php

namespace App\Event\Kernel\Listener;

use App\Repository\UserRepository;
use App\Storage\UserStorage;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class StoreUserToStorageListener
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserStorage
     */
    private $userStorage;

    public function __construct(UserRepository $userRepository, UserStorage $userStorage)
    {
        $this->userRepository = $userRepository;
        $this->userStorage = $userStorage;
    }

    public function onRequestEvent(RequestEvent $event)
    {
        $request = $event->getRequest();
        if ('json' !== $request->getRequestFormat()) {
            return;
        }

        if (!$request->headers->has('Visitor-ID')) {
            throw new AccessDeniedHttpException();
        }

        $this->userStorage->setUser(
            $this->userRepository->findOrCreateByVisitorId($request->headers->get('Visitor-ID'))
        );
    }
}
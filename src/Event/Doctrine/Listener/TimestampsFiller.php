<?php

namespace App\Event\Doctrine\Listener;

use App\Entity\Timestampable\TimestampableInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;

class TimestampsFiller
{
    public function onFlush(OnFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if (!$this->isTimestampable($entity)) {
                continue;
            }
            $this->setCreatedAt($entity);
            $this->recomputeEntityChangeSet($em, $entity);
        }
    }

    private function recomputeEntityChangeSet(EntityManagerInterface $em, $entity)
    {
        $uow = $em->getUnitOfWork();
        $meta = $em->getClassMetadata(get_class($entity));

        if (!$uow->isEntityScheduled($entity)) {
            $uow->computeChangeSet($meta, $entity);
        }
        $uow->recomputeSingleEntityChangeSet($meta, $entity);
    }

    private function setCreatedAt(TimestampableInterface $entity)
    {
        $entity->setCreatedAt(new \DateTime());
    }

    private function isTimestampable($entity): bool
    {
        return $entity instanceof TimestampableInterface;
    }
}
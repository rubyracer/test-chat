<?php

namespace App\Event\Doctrine\Subscriber;

use App\Entity\ChatMember;
use App\Entity\ChatMessage;
use App\Factory\DTO\ChatMemberDTOFactory;
use App\Factory\DTO\ChatMessageDTOFactory;
use App\Service\WsEmitter;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;

class ChatSubscriber implements EventSubscriber
{
    const HANDLERS = [
        ChatMember::class => 'chatMemberHandler',
        ChatMessage::class => 'chatMessageHandler',
    ];

    /**
     * @var WsEmitter
     */
    private $wsEmitter;

    /**
     * @var ChatMessageDTOFactory
     */
    private $chatMessageDTOFactory;

    /**
     * @var ChatMemberDTOFactory
     */
    private $chatMemberDTOFactory;

    public function __construct(
        WsEmitter $wsEmitter,
        ChatMessageDTOFactory $chatMessageDTOFactory,
        ChatMemberDTOFactory $chatMemberDTOFactory
    )
    {
        $this->wsEmitter = $wsEmitter;
        $this->chatMessageDTOFactory = $chatMessageDTOFactory;
        $this->chatMemberDTOFactory = $chatMemberDTOFactory;
    }

    public function getSubscribedEvents()
    {
        return [
            'postPersist',
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->handle($args);
    }

    private function handle(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $handler = self::HANDLERS[get_class($entity)] ?? null;

        if ($handler && is_callable([$this, $handler])) {
            $this->$handler($args, $entity);
        }
    }

    private function chatMemberHandler(LifecycleEventArgs $args, ChatMember $chatMember)
    {
        $chat = $chatMember->getChat();

        $memberDTO = $this->chatMemberDTOFactory->create($chatMember);

        $this->wsEmitter->emit('member', $chat, [
            'chat' => $chat->getId(),
            'member' => $memberDTO,
        ]);
    }

    private function chatMessageHandler(LifecycleEventArgs $args, ChatMessage $message)
    {
        $chat = $message->getChat();

        $messageDTO = $this->chatMessageDTOFactory->create($message);

        $this->wsEmitter->emit('message', $chat, [
            'chat' => $chat->getId(),
            'message' => $messageDTO,
        ]);
    }
}
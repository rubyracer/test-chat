<?php

namespace App\Storage;

use App\Entity\User;

class UserStorage
{
    /**
     * @var User|null
     */
    private $user;

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }
}
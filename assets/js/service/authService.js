export const setVisitorID = () => {
  if (!localStorage.getItem('visitorID')) {
    let visitorID = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)

    localStorage.setItem('visitorID', visitorID)
  }
};

export const getVisitorID = () => {
  return localStorage.getItem('visitorID')
}

export const isMe = (visitorId) => {
  return localStorage.getItem('visitorID') === visitorId
}
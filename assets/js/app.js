/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
import {setVisitorID} from "./service/authService";

require('../css/app.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');
import Vue from 'vue';
import Chats from './components/Chats';
import Chat from './components/Chat';

setVisitorID()

new Vue({
  el: '#app',
  components: {Chats, Chat}
}).$mount('#app');


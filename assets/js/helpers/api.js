import axios from 'axios'
import {getVisitorID} from "../service/authService";

const baseUrl = '/api';

const instance = axios.create({
  baseURL: baseUrl,
  headers: {'Visitor-ID': getVisitorID()}
});

export const getChats = () => {
  const url = `/chats`;

  return instance.get(url).then(data => data);
};

export const createChat = () => {
  const url = `/chats`;

  return instance.post(url).then(data => data);
};

export const getChat = (chatId) => {
  const url = `/chats/${chatId}`;

  return instance.get(url).then(data => data);
};

export const joinToChat = (chatId) => {
  const url = `/chats/${chatId}/join`;

  return instance.post(url).then(data => data);
};

export const sendMessage = (chatId, message) => {
  const url = `/chats/${chatId}/message`;

  return instance.post(url, {message: message}).then(data => data);
};

export default instance;

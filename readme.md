- docker-compose up
- composer install
- yarn install
- yarn run dev
- docker-compose exec mysql mysql -u root -p
    * create database app; 
- docker-compose exec php php bin/console doctrine:migrations:migrate

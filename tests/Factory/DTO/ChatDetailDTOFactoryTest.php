<?php

namespace App\Tests\Factory\DTO;

use App\DTO\Chat\ChatDetailDTO;
use App\Factory\ChatFactory;
use App\Factory\DTO\ChatDetailDTOFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ChatDetailDTOFactoryTest extends WebTestCase
{
    public function testCreate()
    {
        self::bootKernel();

        $container = self::$container;
        $chatFactory = $container->get(ChatFactory::class);
        $chatDTODetailFactory = $container->get(ChatDetailDTOFactory::class);

        $chat = $chatFactory->create();

        $result = $chatDTODetailFactory->create($chat);

        $this->assertInstanceOf(ChatDetailDTO::class, $result);
    }
}
<?php

namespace App\Tests\Factory;

use App\Entity\Chat;
use App\Factory\ChatFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ChatFactoryTest extends WebTestCase
{
    public function testCreate()
    {
        self::bootKernel();

        $container = self::$container;
        $chatFactory = $container->get(ChatFactory::class);

        $result = $chatFactory->create();

        $this->assertInstanceOf(Chat::class, $result);
    }
}
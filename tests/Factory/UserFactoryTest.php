<?php

namespace App\Tests\Factory;

use App\Entity\Chat;
use App\Entity\User;
use App\Factory\ChatFactory;
use App\Factory\UserFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserFactoryTest extends WebTestCase
{
    public function testCreate()
    {
        self::bootKernel();

        $container = self::$container;
        $userFactory = $container->get(UserFactory::class);

        $visitorId = uniqid();
        $result = $userFactory->create($visitorId);

        $this->assertInstanceOf(User::class, $result);
        $this->assertEquals($visitorId, $result->getVisitorId());
    }
}